package contunify.ui;

import javax.swing.*;

@SuppressWarnings("serial")
public class Contunify extends JFrame {

	public Contunify() {
		// Basic setup for the window
		this.setSize(800, 600);
		this.setLocationRelativeTo(null);
		this.setTitle("Contunify - Unifies your messy contacts");

		// Menu
		JMenuBar menuBar = new JMenuBar();

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		// Add the menu items
		JMenuItem mnImport = new JMenuItem("Import");
		mnFile.add(mnImport);
		JMenuItem mnExport = new JMenuItem("Export");
		mnFile.add(mnExport);
		
		// this is necessary for the menu bar to show up
		this.setJMenuBar(menuBar);

		// Closing operation
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Window setup must come before setVisible
		this.setVisible(true);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		if (System.getProperty("os.name").contains("Mac")) {
			System.out.println("I'm a mac");
		}

		Contunify conty = new Contunify();
	}

}
